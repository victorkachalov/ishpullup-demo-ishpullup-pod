# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Типовой шаблон для имплементирования ISHPullUP (будет доступен интерактивный ползунок при смахивании снизу вверх

### How do I get set up? ###

Для работы нужно взять за основу болванку ViewController (инстанс нашего ISHPullUPViewController)
В ContentVC нужно подписываемся на протокол  ContentDelegate и реализовать метод добавления ползунка снизу
В BottomVC подписываемся на протоколы SizingDelegate и StateDelegate и имплементируем методы для работы с ползунком