//
//  ViewController.swift
//  ISHPullUp Demo
//
//  Created by iOS_Razrab on 28/09/2017.
//  Copyright © 2017 iOS_Razrab. All rights reserved.
//

//Создаем инстанс VC который будет собираться из BottomVC и ContentVC
    /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */

import UIKit
import ISHPullUp

class ViewController: ISHPullUpViewController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }
    
    private func commonInit() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let contentVC = storyBoard.instantiateViewController(withIdentifier: "content") as? ContentController else { return }
        guard let bottomVC = storyBoard.instantiateViewController(withIdentifier: "bottom") as? BottomController else { return }
        contentViewController = contentVC
        bottomViewController = bottomVC
        bottomVC.pullUpController = self
        contentDelegate = contentVC
        sizingDelegate = bottomVC
        stateDelegate = bottomVC
    }

}

    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
