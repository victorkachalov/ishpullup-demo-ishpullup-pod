//
//  ContentController.swift
//  ISHPullUp Demo
//
//  Created by iOS_Razrab on 28/09/2017.
//  Copyright © 2017 iOS_Razrab. All rights reserved.
//

import UIKit
import ISHPullUp

class ContentController: UIViewController, ISHPullUpContentDelegate {

    @IBOutlet private weak var rootView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    func pullUpViewController(_ vc: ISHPullUpViewController, update edgeInsets: UIEdgeInsets, forContentViewController _: UIViewController) {
        rootView.layoutIfNeeded()
    }

    
}
